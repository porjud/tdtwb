//
//  ViewController.m
//  satellitesnapshot
//
//  Created by Judd Morrissey on 7/22/16.
//  Copyright © 2016 Judd Morrissey. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;


@interface ViewController ()
@property (strong, nonatomic)           SCNScene    *scene;
@property (strong, nonatomic)           UIImage     *satelliteTexture;
@property (strong, nonatomic)           SCNText     *textAsset;
@property (strong, nonatomic)           SCNNode     *node;

@property (strong, nonatomic)           SCNNode     *cameraNode;
@property (strong, nonatomic)           SCNView     *sceneView;

// Video Preview
@property (strong, nonatomic)           AVCaptureSession     *captureSession;
@property (strong, nonatomic)           AVCaptureVideoPreviewLayer  *captureLayer;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    SCNScene *scene = [SCNScene scene];
    self.scene=scene;
    
    CGRect frame = [[UIScreen mainScreen] bounds];
    self.sceneView = [[SCNView alloc] initWithFrame:frame];
    self.sceneView.scene = scene;
    self.sceneView.allowsCameraControl=YES;
    
    
    self.sceneView.backgroundColor = [UIColor clearColor];
    self.sceneView.opaque=NO;
   
   
    [self startCamera];
    [self.view addSubview:self.sceneView];
    
    SCNCamera *camera = [SCNCamera camera];
    camera.xFov = 45;
    camera.yFov = 45;
 
    
    
    SCNNode *cameraNode = [SCNNode node];
    self.cameraNode = cameraNode;
    self.cameraNode.camera = camera;
    self.cameraNode.position = SCNVector3Make(0, 0, 100);
     self.cameraNode.camera.zFar = 1000000000;
    [self.sceneView.scene.rootNode addChildNode:cameraNode]; // Place camera in the scene
    
    

    
 
  
   
    
    [self createTextNode];
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(60.409005, 5.315169);
  
    [self mapSnapshot:location sceneText:self.textAsset];
    // Do any additional setup after loading the view, typically from a nib.
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) mapSnapshot:(CLLocationCoordinate2D)location sceneText:(SCNText*)text  {
    MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
    options.region = [self calculateMapRegion:location];
    
    options.scale = [UIScreen mainScreen].scale;
    options.size = self.view.frame.size;
    options.mapType = MKMapTypeSatellite;
    
    MKMapSnapshotter *snapshotter = [[MKMapSnapshotter alloc] initWithOptions:options];
    [snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
        MKMapSnapshot *mapSnapshot = snapshot;
        self.satelliteTexture = mapSnapshot.image;
        NSLog(@"Done");
    
        //return self.satelliteTexture;
      // UIImageView *imageView = [[UIImageView alloc] initWithImage:mapSnapshot.image];
      // imageView.frame = self.view.bounds;
        text.firstMaterial.diffuse.contents = self.satelliteTexture;
        text.string = @"The word boy rises";
      //[self.view addSubview:imageView];
    }];
   
}

- (void)createTextNode
{
    // instatiate SCNText
    self.textAsset = [SCNText textWithString:@"BUØY" extrusionDepth:0.2];
    
   
    self.textAsset.font = [UIFont fontWithName:@"GillSans" size:(8.5)];
    self.textAsset.flatness = 0.1; // a lower value for smoother text”
    self.textAsset.alignmentMode = kCAAlignmentCenter;
   // self.textAsset.
    self.node = [SCNNode nodeWithGeometry:self.textAsset];
    self.node.position = SCNVector3Make(-40.0, 0.0, 0.0);
    [self.sceneView.scene.rootNode addChildNode:self.node];
}

- (MKCoordinateRegion)calculateMapRegion:(CLLocationCoordinate2D)location {
    srand48(time(0));
    double miles = drand48()*10;
    NSLog(@"%f",miles);
    double scalingFactor = ABS( (cos(2 * M_PI * location.latitude / 360.0) ));
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor * 69.0);
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = location;
    return region;

}




-(void)startCamera {
AVCaptureDevice *camera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
if (!camera) {
    NSLog(@"ERROR - could not access camera");
    return;
}
self.captureSession = [[AVCaptureSession alloc] init];
AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:camera error:nil];
[self.captureSession addInput:newVideoInput];

self.captureLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];

// Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    [self.captureSession startRunning];
});

CGRect frame = CGRectMake(self.view.bounds.origin.x,
                          self.view.bounds.origin.y,
                          self.view.bounds.size.width,
                          self.view.bounds.size.height);

self.captureLayer.frame = frame;
self.captureLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;

NSLog(@"CameraViewController self.view.bounds: x: %.2f y: %.2f w:%.2f h:%.2f", self.view.bounds.origin.x,
      self.view.bounds.origin.y,
      self.view.bounds.size.width,
      self.view.bounds.size.height);

[self.view.layer addSublayer:self.captureLayer];
}



@end
