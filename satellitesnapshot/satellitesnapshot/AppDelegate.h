//
//  AppDelegate.h
//  satellitesnapshot
//
//  Created by Judd Morrissey on 7/22/16.
//  Copyright © 2016 Judd Morrissey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

