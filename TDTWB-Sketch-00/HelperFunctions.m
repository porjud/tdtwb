//
//  HelperFunctions.m
//  CollocationsMockup
//
//  Created by Abraham Avnisan on 9/24/15.
//  Copyright (c) 2015 Abraham Avnisan. All rights reserved.
//

#pragma mark - CUSTOM MACROS

#ifndef HF_MAX
#define HF_MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif

#ifndef HF_MIN
#define HF_MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif

#import "HelperFunctions.h"

#pragma mark - FUNCTIONS PERTAINING TO RANDOM NUMBERS

void hfSeedRandom()
{
    srand(CFAbsoluteTimeGetCurrent());
}

float hfRandom(float x, float y) {
    
    //    NSLog(@"hfRandom X: %f Y: %f", x, y);
    //    NSLog(@"random test IN HFRANDOM: %f", (rand()/(RAND_MAX + 1.0f)) * 2.0f - 1.0f);
    
    float high = 0;
    float low = 0;
    float randNum = 0;
    
    // if there is no range, return the value
    if (x == y) return x; 			// float == ?, wise? epsilon?
    high = HF_MAX(x,y);
    low = HF_MIN(x,y);
    randNum = low + ((high-low) * rand()/(RAND_MAX + 1.0f));
    return randNum;
}

float hfRandomf() {
    float randNum = 0;
    randNum = (rand()/(RAND_MAX + 1.0f)) * 2.0f - 1.0f;
    return randNum;
}

float hfRandomPosF() {
    float randNum = 0;
    float numerator = rand();
    randNum = numerator / RAND_MAX;
    return randNum;
}
#pragma mark - MAPPING FUNCTIONS

float hfMap(float input, float inputMin, float inputMax, float outputMin, float outputMax)
{
    float mappedValue = outputMin + (outputMax - outputMin) * (input - inputMin) / (inputMax - inputMin);
    return mappedValue;
}


float hfMapClamped(float input, float inputMin, float inputMax, float outputMin, float outputMax)
{
    float mappedValue = outputMin + (outputMax - outputMin) * (input - inputMin) / (inputMax - inputMin);
    if (mappedValue > outputMax) mappedValue = outputMax;
    if (mappedValue < outputMin) mappedValue = outputMin;
    return mappedValue;
}

























