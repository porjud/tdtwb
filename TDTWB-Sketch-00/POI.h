//
//  LocationTranslation.h
//  TDTWB-Sketch-00
//
//  Created by Abraham Avnisan on 6/14/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;
@import SceneKit;

// define and structs

#define SCALE_FACTOR .1



typedef struct LLA
{
    double latitude;
    double longitude;
    double altitude;
} LLA;

typedef struct ENU
{
    double e;
    double n;
    double u;
} ENU;

// c functions for enu conversion

extern ENU ENUMake(double e, double n, double u);
extern SCNVector3 ENUtoXYZ(ENU enu);
extern ENU XYZtoENU(SCNVector3 XYZ);


@interface POI : NSObject

// PROPERTIES

// General
@property (strong, nonatomic)   NSString                *label;

// Geo Location
@property (nonatomic)           LLA                     referenceLLA;
@property (nonatomic)           LLA                     POILLA;
@property (nonatomic)           ENU                     POIENU;

// 3D Location (in SceneKit)
@property (nonatomic)           SCNVector3              POIXYZ;

// 3D Properties
@property (strong, nonatomic)   SCNText                 *textAsset;
@property (strong, nonatomic)   SCNNode                 *node;
@property (nonatomic)           SCNVector3              rotation;

// METHODS

// Initializers
- (instancetype)initWithReference:(LLA)reference POI:(LLA)POI label:(NSString *)label textScale:(double)textScale;
- (instancetype)initWithReference:(LLA)reference remoteOrigin:(LLA)remoteOrigin POIEnu:(ENU)POIEnu label:(NSString *)label textScale:(double)textScale;


@end
