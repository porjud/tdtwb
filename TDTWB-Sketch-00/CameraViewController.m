//
//  ViewController.m
//  TDTWB-Sketch-00
//
//  Created by Abraham Avnisan on 6/10/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import "CameraViewController.h"
#import "DSPFilter.h"
#import "POI.h"
#import "HelperFunctions.h"
#import <stdlib.h>

@import CoreMotion;
@import AVFoundation;
@import GLKit;

@interface CameraViewController ()

// Video Preview
@property (strong, nonatomic)           AVCaptureSession            *captureSession;
@property (strong, nonatomic)           AVCaptureVideoPreviewLayer  *captureLayer;

@end

@implementation CameraViewController

#pragma mark - Setup
- (void)setup
{
    // setup camera
    [self startCamera];

}
#pragma mark - camera preview layer
- (void)startCamera
{
    AVCaptureDevice *camera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (!camera) {
        NSLog(@"ERROR - could not access camera");
        return;
    }
    self.captureSession = [[AVCaptureSession alloc] init];
    AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:camera error:nil];
    [self.captureSession addInput:newVideoInput];
    
    self.captureLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    
    // Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.captureSession startRunning];
    });
    
    CGRect frame = CGRectMake(self.view.bounds.origin.x,
                              self.view.bounds.origin.y,
                              self.view.bounds.size.width,
                              self.view.bounds.size.height);
    
    self.captureLayer.frame = frame;
    self.captureLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    NSLog(@"CameraViewController self.view.bounds: x: %.2f y: %.2f w:%.2f h:%.2f", self.view.bounds.origin.x,
          self.view.bounds.origin.y,
          self.view.bounds.size.width,
          self.view.bounds.size.height);
    
    [self.view.layer addSublayer:self.captureLayer];
    
}
#pragma mark - Inherited Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setup];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
