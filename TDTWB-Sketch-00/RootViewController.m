//
//  RootViewController.m
//  TDTWB-Sketch-00
//
//  Created by Abraham Avnisan on 7/18/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import "RootViewController.h"
#import "CameraViewController.h"
#import "POIViewController.h"

@interface RootViewController ()

@property (strong, nonatomic) CameraViewController *cameraVC;
@property (strong, nonatomic) POIViewController *POIVC;

@end

@implementation RootViewController
#pragma mark - lazy instantiation
- (CameraViewController *)cameraVC
{
    if (!_cameraVC) {
        _cameraVC = [[CameraViewController alloc] init];
    }
    return _cameraVC;
}
- (POIViewController *)POIVC
{
    if (!_POIVC) {
        _POIVC = [[POIViewController alloc] init];
    }
    return _POIVC;
}
#pragma mark - setup
- (void)setup
{
    // add child views to root view
    [self.view addSubview:self.cameraVC.view];
    [self.view addSubview:self.POIVC.view];


}
#pragma mark - inherited methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

@end
