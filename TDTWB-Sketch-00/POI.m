//
//  LocationTranslation.m
//  TDTWB-Sketch-00
//
//  Created by Abraham Avnisan on 6/14/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import "POI.h"

#pragma mark Geodetic utilities declaration

// math stuff taken from pARk example code

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)

#define WGS84_A	(6378137.0)				// WGS 84 semi-major axis constant in meters
#define WGS84_E (8.1819190842622e-2)	// WGS 84 eccentricity

// Converts latitude, longitude to ECEF coordinate system
void latLonToEcef(double lat, double lon, double alt, double *x, double *y, double *z);

// Converts ECEF to ENU coordinates centered at given lat, lon
void ecefToEnu(double lat, double lon, double xRef, double yRef, double zRef, double xPOI, double yPOI, double zPOI, double *e, double *n, double *u);

// Converts ENU of a POI (in realtion to a reference point) to ECEF coordinates
void enuToEcef(double lat, double lon, double xRef, double yRef, double zRef, double *xPOI, double *yPOI, double *zPOI, double e, double n, double u);

// Converts from ECEF to LLA
void ecefToLLA(double x, double y, double z, double *lat, double *lon, double *alt);


@interface POI ()

// ECEF for Reference Point
@property (nonatomic) double xRef;
@property (nonatomic) double yRef;
@property (nonatomic) double zRef;

// ECEF for POI
@property (nonatomic) double xPOI;
@property (nonatomic) double yPOI;
@property (nonatomic) double zPOI;

//for text manipulation
@property (nonatomic) double textScale;

@end

@implementation POI

#pragma mark - lazy instantiation

@synthesize rotation = _rotation;

- (void)setRotation:(SCNVector3)rotation
{
    _rotation = rotation;
    SCNVector3 eulerAngles = SCNVector3Make(DEGREES_TO_RADIANS(rotation.x), DEGREES_TO_RADIANS(rotation.y), DEGREES_TO_RADIANS(rotation.z));
    self.node.eulerAngles = eulerAngles;
}
- (SCNVector3)rotation
{
    SCNVector3 euler = self.node.eulerAngles;
    SCNVector3 rotationDegrees = SCNVector3Make(RADIANS_TO_DEGREES(euler.x), RADIANS_TO_DEGREES(euler.y), RADIANS_TO_DEGREES(euler.z));
    return rotationDegrees;
}
- (void)setReferenceLLA:(LLA)referenceLLA
{
    _referenceLLA = referenceLLA;
    
    // convert reference to ECEF
    double xRef, yRef, zRef;
    latLonToEcef(referenceLLA.latitude, referenceLLA.longitude, referenceLLA.altitude, &xRef, &yRef, &zRef);

    self.xRef = xRef;
    self.yRef = yRef;
    self.zRef = zRef;
}
- (void)setPOILLA:(LLA)POILLA
{
    _POILLA = POILLA;
    
    // convert POI to ECEF
    double xPOI, yPOI, zPOI;
    latLonToEcef(POILLA.latitude, POILLA.longitude, POILLA.altitude, &xPOI, &yPOI, &zPOI);
    
    self.xPOI = xPOI;
    self.yPOI = yPOI;
    self.zPOI = zPOI;

}

#pragma mark - designated initializer
- (instancetype)initWithReference:(LLA)reference POI:(LLA)POI label:(NSString *)label textScale:(double)textScale
{
    self = [super init];
    if (self) {
        
        self.label = label;
        self.referenceLLA = reference;
        self.POILLA = POI;
    
        
        // convert ecefToENU with respect to the reference and POI
        double e, n, u;
        ecefToEnu(reference.latitude, reference.longitude, self.xRef, self.yRef, self.zRef, self.xPOI, self.yPOI, self.zPOI, &e, &n, &u);
        
        self.POIENU = ENUMake(e, n, u);
        
        self.POIXYZ = ENUtoXYZ(self.POIENU);
        
        self.textScale = textScale;
        
        [self createTextNode];
        
        // log description
        NSLog(@"%@", self);
    }
    return self;
}
- (instancetype)initWithReference:(LLA)reference remoteOrigin:(LLA)remoteOrigin POIEnu:(ENU)POIEnu label:(NSString *)label  textScale:(double)textScale
{
    self.referenceLLA = remoteOrigin;
    
    // convert POIEnu to ECEF
    double xPOI, yPOI, zPOI;
    enuToEcef(  self.referenceLLA.latitude,   self.referenceLLA.longitude, self.xRef, self.yRef, self.zRef, &xPOI, &yPOI, &zPOI, POIEnu.e, POIEnu.n, POIEnu.u);

    // convert POI ECEF to LLA
    LLA POILLA;
    ecefToLLA(xPOI, yPOI, zPOI, &POILLA.latitude, &POILLA.longitude, &POILLA.altitude );
    
    POILLA.latitude = RADIANS_TO_DEGREES(POILLA.latitude);
    POILLA.longitude = RADIANS_TO_DEGREES(POILLA.longitude);
    //added 7.20
    POILLA.altitude = RADIANS_TO_DEGREES(POILLA.altitude);
    
    NSLog(@"\nreference: %f, %f, %f \nPOILLA: %f, %f, %f", reference.latitude,  reference.longitude, reference.altitude, POILLA.latitude, POILLA.longitude, POILLA.altitude);
    
   
    
    
   return [self initWithReference:reference POI:POILLA label:label textScale:textScale];
  
    
}
#pragma mark - helper methods
- (void)createTextNode
{
    // instatiate SCNText
    self.textAsset = [SCNText textWithString:self.label extrusionDepth:0.2];
    
    self.textAsset.firstMaterial.diffuse.contents = [UIImage imageNamed:@"JenJuddAbe"];
    self.textAsset.font = [UIFont fontWithName:@"GillSans" size:(3.5*self.textScale)];
    self.textAsset.flatness = 0.1; // a lower value for smoother text”
    self.textAsset.alignmentMode = kCAAlignmentCenter;

    self.node = [SCNNode nodeWithGeometry:self.textAsset];
    self.node.position = self.POIXYZ;

   

}

#pragma mark - inherited methods
- (NSString *)description
{
    NSString *description = [NSString stringWithFormat:@"\n*******************\nPOI '%@'", self.label];
    description = [description stringByAppendingFormat:@"\nLLA = L: %012.5f L: %012.5f A: %012.5f\nENU = e: %012.5f n: %012.5f u: %012.5f\nXYZ = x: %012.5f y: %012.5f z: %012.5f \nxRef: %012.5f, yRef: %012.5f, zRef: %012.5f",
                                            self.POILLA.latitude, self.POILLA.longitude, self.POILLA.altitude,
                                            self.POIENU.e, self.POIENU.n, self.POIENU.u,
                                            self.POIXYZ.x, self.POIXYZ.y, self.POIXYZ.z,
                                            self.xRef, self.yRef, self.zRef];
    return description;
}

@end

#pragma mark Geodetic utilities definition

// References to ECEF and ECEF to ENU conversion may be found on the web.

// Converts latitude, longitude to ECEF coordinate system
void latLonToEcef(double lat, double lon, double alt, double *x, double *y, double *z)
{
    double clat = cos(DEGREES_TO_RADIANS(lat));
    double slat = sin(DEGREES_TO_RADIANS(lat));
    double clon = cos(DEGREES_TO_RADIANS(lon));
    double slon = sin(DEGREES_TO_RADIANS(lon));
    
    double N = WGS84_A / sqrt(1.0 - WGS84_E * WGS84_E * slat * slat);
    
    *x = (N + alt) * clat * clon;
    *y = (N + alt) * clat * slon;
    *z = (N * (1.0 - WGS84_E * WGS84_E) + alt) * slat;
}

// Converts ECEF to ENU coordinates centered at given lat, lon
void ecefToEnu(double lat, double lon, double xRef, double yRef, double zRef, double xPOI, double yPOI, double zPOI, double *e, double *n, double *u)
{
    double clat = cos(DEGREES_TO_RADIANS(lat));
    double slat = sin(DEGREES_TO_RADIANS(lat));
    double clon = cos(DEGREES_TO_RADIANS(lon));
    double slon = sin(DEGREES_TO_RADIANS(lon));
    double dx = xPOI - xRef;
    double dy = yPOI - yRef;
    double dz = zPOI - zRef;
    
    *e = -slon * dx  + clon * dy;
    *n = -slat * clon * dx - slat * slon * dy + clat * dz;
    *u = clat * clon * dx + clat * slon * dy + slat * dz;
}
// Converts ENU of a POI (in realtion to a reference point) to ECEF coordinates
void enuToEcef(double lat, double lon, double xRef, double yRef, double zRef, double *xPOI, double *yPOI, double *zPOI, double e, double n, double u)
{
    double clat = cos(DEGREES_TO_RADIANS(lat));
    double slat = sin(DEGREES_TO_RADIANS(lat));
    double clon = cos(DEGREES_TO_RADIANS(lon));
    double slon = sin(DEGREES_TO_RADIANS(lon));
    
    *xPOI = (-slon * e) + (-slat * clon * n) + (clat * clon * u) + xRef;
    *yPOI = (clon * e)  + (-slat * slon * n) + (clat * slon * u) + yRef;
    *zPOI = (0 * e)     + (clat * n)         + (slat * u)        + zRef;

}
// Converts from ECEF to LLA
void ecefToLLA(double x, double y, double z, double *lat, double *lon, double *alt)
{
    // converted from this java snippet: https://gist.github.com/klucar/1536194
    
    double a = WGS84_A; // radius
    double e = WGS84_E;  // eccentricity
    
    double asq = pow(a, 2);
    double esq = pow(e, 2);
    
    double b = sqrt(asq * (1 - esq));
    double bsq = pow(b, 2);
    double ep = sqrt((asq - bsq) / bsq);
    double p = sqrt((pow(x, 2) + pow(y, 2)));
    double th = atan2(a * z, b * p);
    
    *lon = atan2(y, x);
    *lat = atan2( (z + (pow(ep, 2) * b * pow(sin(th), 3))), (p - esq * a * pow(cos(th), 3)));
    double N = a / (sqrt(1 - esq * pow(sin(*lat), 2)));
    *alt = p / cos(*lat) - N;
    
    *lon = fmod(*lon, 2 * M_PI);
    
    // correction for altitude near poles left out.
    
}
ENU ENUMake(double e, double n, double u)
{
    ENU enu = {e, n, u};
    return enu;
}
SCNVector3 ENUtoXYZ(ENU enu)
{
    double x, y, z;
    x = enu.e * SCALE_FACTOR;
    y = enu.u * SCALE_FACTOR;
    z = enu.n * -1 * SCALE_FACTOR;
    
    return SCNVector3Make(x, y, z);
}
ENU XYZtoENU(SCNVector3 XYZ)
{
    ENU enu = { XYZ.x / SCALE_FACTOR, (XYZ.z * -1)  / SCALE_FACTOR, XYZ.y / SCALE_FACTOR };
    return enu;
}