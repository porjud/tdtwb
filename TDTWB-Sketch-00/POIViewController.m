//
//  POIViewController.m
//  TDTWB-Sketch-00
//
//  Created by Abraham Avnisan on 7/18/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import "POIViewController.h"
@import CoreMotion;
@import GLKit;
#import "POI.h"

#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define IS_SITE_SPECIFIC YES
const LLA siteLLA = {
    //.latitude = 41.921216,
    //.longitude = -87.652298,
    .latitude = 41.897609,
    .longitude = -87.690227,
    .altitude = 0.0
};



@interface POIViewController ()

// Views and Such
@property (strong, nonatomic)           SCNNode                     *cameraNode;
@property (strong, nonatomic)           SCNView                     *POISceneView;


// Device Motion && Location
@property (strong, nonatomic)           CMMotionManager             *motionManager;
@property (strong, nonatomic)           CLLocationManager           *locationManager;
@property (strong, nonatomic)           CLHeading                   *heading;
@property (nonatomic)                   BOOL                        firstHeadingUpdateReceived;

// POI's
@property (strong, nonatomic)           NSMutableArray              *POIs;
@property (strong, nonatomic)           SCNNode                     *POIParentNode;

@end

@implementation POIViewController


#pragma mark - Lazy Instantiation
- (CMMotionManager *)motionManager
{
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc] init];
    }
    return _motionManager;
}
- (CLLocationManager *)locationManager
{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
    }
    return _locationManager;
}
- (NSMutableArray *)POIs
{
    if (!_POIs) {
        _POIs = [[NSMutableArray alloc] init];
    }
    return _POIs;
}
- (SCNNode *)POIParentNode
{
    if (!_POIParentNode) {
        _POIParentNode = [SCNNode node];
    }
    return _POIParentNode;
}
- (SCNView *)POISceneView
{
    if (!_POISceneView) {
        CGRect frame = CGRectMake(self.view.bounds.origin.x,
                                  self.view.bounds.origin.y,
                                  self.view.bounds.size.width,
                                  self.view.bounds.size.height);
        
        _POISceneView = [[SCNView alloc] initWithFrame:frame];
    }
    return _POISceneView;
}
#pragma mark - Setup
- (void)setup
{
    NSLog(@"IS_SITE_SPECIFIC: %d", IS_SITE_SPECIFIC);
    // views
    [self setupSceneView];
    
    // general
    self.firstHeadingUpdateReceived = NO;
    
    // setup location manager
    [self.locationManager requestWhenInUseAuthorization];
    self.locationManager.delegate = self;
    self.locationManager.headingFilter = kCLHeadingFilterNone;
    [self.locationManager startUpdatingHeading];
    
    
    // setup motion manager
    self.motionManager.showsDeviceMovementDisplay = YES;
    self.motionManager.deviceMotionUpdateInterval = 1.0 / 60.0;
    [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryZVertical
                                                            toQueue:[NSOperationQueue mainQueue]
                                                        withHandler:^
     (CMDeviceMotion * motion, NSError *error) {
         
         // for convenience, this block will take care of other aspects of our
         // "update" loop
         
         // get current device attitude as a quaternion
         CMQuaternion attitude = motion.attitude.quaternion;
         
         // translate to GLQuaternion
         GLKQuaternion attitudeGLQuaternion = GLKQuaternionMake(attitude.x, attitude.y, attitude.z, attitude.w);
         
         // create a corrective quaternion
         GLKQuaternion correctiveQuaternion = GLKQuaternionMakeWithAngleAndAxis(-M_PI_2, 1, 0, 0);
         
         // multiply corrective quaternion by current quaternion
         GLKQuaternion finalGLQuaternion = GLKQuaternionMultiply(correctiveQuaternion, attitudeGLQuaternion);
         
         // assign final quaternion to cameraNode.orientation
         self.cameraNode.orientation = SCNVector4Make(finalGLQuaternion.x, finalGLQuaternion.y, finalGLQuaternion.z, finalGLQuaternion.w);;
         
         
     }];
    
    // setup POI's
    [self setupReferenceAndPOIsUsingLLAs];
    [self setupPOIENUsWithCurrentLocation];
    [self createPOICircleWithRadius:600.0 withSatellites:6 atAltitude:100.0 aroundAxis:1];
    [self createPOIRadialPoint:500.0 withSatellites:6 atAltitude:385.0 atPosition:3 aroundAxis:1];
    [self createPOIRadialPoint:500.0 withSatellites:6 atAltitude:585.0 atPosition:1 aroundAxis:1];
    [self setupReferenceTexts];
    
}
- (void)setupSceneView
{
    
    // grab the scene file and load it
    SCNScene *scene = [SCNScene sceneNamed:@"scene.scn"];
    
    // grab the camera node and assign it to the cameraNode property
    self.cameraNode = [scene.rootNode childNodeWithName:@"camera" recursively:YES];
    
    // set clipping
    self.cameraNode.camera.zFar = 1000000000;
    
    // add the POIParentNode to the scene
    [scene.rootNode addChildNode:self.POIParentNode];
    
    // assign the scene to our SCNView property's scene
    self.POISceneView.scene = scene;
    
    // make scene background transparent
    self.POISceneView.opaque = NO;
    self.POISceneView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0];
    
    // set view properties
    self.POISceneView.playing = YES;
    self.POISceneView.showsStatistics = YES;
    self.POISceneView.allowsCameraControl = YES;
    
    self.view = self.POISceneView;
    
}
#pragma mark - Location Manager Delegate Method
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    self.heading = newHeading;
    
    if (!self.firstHeadingUpdateReceived) {
        self.firstHeadingUpdateReceived = YES;
        NSLog(@"my current location: %f %f", manager.location.coordinate.latitude, manager.location.coordinate.longitude);
        
        [self.POIParentNode runAction:[SCNAction rotateToX:0 y: DEGREES_TO_RADIANS(self.heading.trueHeading) z:0 duration:1.5]];
    }
    
}
#pragma mark - Main Methods
- (void)createPOICircleWithRadius:(double)r withSatellites:(NSUInteger)satellites atAltitude:(double)altitude aroundAxis:(NSUInteger)axis
{
    
    LLA referenceLLA;
    referenceLLA.latitude = self.locationManager.location.coordinate.latitude;
    referenceLLA.longitude = self.locationManager.location.coordinate.longitude;
    referenceLLA.altitude = DEGREES_TO_RADIANS(altitude);
    
     LLA originLLA;
    if(IS_SITE_SPECIFIC) {
        originLLA = siteLLA;
        originLLA.altitude = DEGREES_TO_RADIANS(altitude);
    } else {
         originLLA = referenceLLA;
    }
    
    NSArray *content = [self radialText];
    NSMutableArray *ENUs = [[NSMutableArray alloc] init];
    NSMutableArray *Labels = [[NSMutableArray alloc] init];
    int index = 0;
    for (NSUInteger i = 360; i > 0; i -= 360/[content count]) {
        
        double e, n, u;
        NSString *axisStr;
        if (axis == 1) {
            
            e = r * cos(DEGREES_TO_RADIANS(i));
            n = r * sin(DEGREES_TO_RADIANS(i));
            u = 0;
            axisStr = content[index];
        } else if (axis == 0) {
            
            e = 0;
            n = r * cos(DEGREES_TO_RADIANS(i));
            u = r * sin(DEGREES_TO_RADIANS(i));
            axisStr = @"E-";
        } else if (axis == 2) {
            
            e = r * cos(DEGREES_TO_RADIANS(i));
            n = 0;
            u = r * sin(DEGREES_TO_RADIANS(i));
            axisStr = @"N-";
        }
        index++;
        
        ENU enu = {e, n, u};
        [ENUs addObject:[NSValue valueWithBytes:&enu objCType:@encode(ENU)]];
        
        NSString *label = [NSString stringWithFormat:@"%@", axisStr];
        [Labels addObject:label];
    }
    
    for (NSUInteger i = 0; i < [ENUs count]; i++) {
        
        // create a POI object
        ENU POIENU;
        [ENUs[i] getValue:&POIENU];
      
      
        POI *poi = [[POI alloc] initWithReference:referenceLLA remoteOrigin:originLLA POIEnu:POIENU label:Labels[i] textScale:1.5];
        
        poi.textAsset.firstMaterial.diffuse.contents = [UIColor blackColor];
        
        // create a SCNNode for the POI with the POI textAsset
        SCNNode *POINode = [SCNNode nodeWithGeometry:poi.textAsset];
        
        // set the node's position and rotation (default faces the reference point)
        POINode.position = poi.POIXYZ;
        POINode.eulerAngles = [self EulersToLookAtReferenceFromPoint:poi.POIXYZ];
     
        
        [self.POIParentNode addChildNode:POINode];
//        NSLog(@"adding '%@' to scene", poi.textAsset.string);
        
    }
    
}

- (void)createPOIRadialPoint:(double)r withSatellites:(NSUInteger)satellites atAltitude:(double)altitude atPosition:(int)position aroundAxis:(NSUInteger)axis
{
    
    LLA referenceLLA;
    referenceLLA.latitude = self.locationManager.location.coordinate.latitude;
    referenceLLA.longitude = self.locationManager.location.coordinate.longitude;
    referenceLLA.altitude = DEGREES_TO_RADIANS(altitude);
    
    LLA originLLA;
    if(IS_SITE_SPECIFIC) {
        originLLA = siteLLA;
        originLLA.altitude = DEGREES_TO_RADIANS(altitude);
    } else {
        originLLA = referenceLLA;
    }
    
    NSMutableArray *ENUs = [[NSMutableArray alloc] init];
    NSMutableArray *Labels = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < 360; i += 360/satellites) {
        
        double e, n, u;
        NSString *axisStr;
        if (axis == 1) {
            
            e = r * cos(DEGREES_TO_RADIANS(i));
            n = r * sin(DEGREES_TO_RADIANS(i));
            u = 0;
            axisStr = @"U-";
        } else if (axis == 0) {
            
            e = 0;
            n = r * cos(DEGREES_TO_RADIANS(i));
            u = r * sin(DEGREES_TO_RADIANS(i));
            axisStr = @"E-";
        } else if (axis == 2) {
            
            e = r * cos(DEGREES_TO_RADIANS(i));
            n = 0;
            u = r * sin(DEGREES_TO_RADIANS(i));
            axisStr = @"N-";
        }
        
        ENU enu = {e, n, u};
        [ENUs addObject:[NSValue valueWithBytes:&enu objCType:@encode(ENU)]];
        
        NSString *label = [NSString stringWithFormat:@"%@%lu", axisStr, (long)i];
        [Labels addObject:label];
    }
    
    for (NSUInteger i = 0; i < [ENUs count]; i++) {
        if(i==position) {
            // create a POI object
            ENU POIENU;
            [ENUs[i] getValue:&POIENU];
            
            
            POI *poi = [[POI alloc] initWithReference:referenceLLA remoteOrigin:originLLA POIEnu:POIENU label:Labels[i] textScale:1.5];
            
            // create a SCNNode for the POI with the POI textAsset
            SCNNode *POINode = [SCNNode nodeWithGeometry:poi.textAsset];
            
            // set the node's position and rotation (default faces the reference point)
            POINode.position = poi.POIXYZ;
            POINode.eulerAngles = [self EulersToLookAtReferenceFromPoint:poi.POIXYZ];
            
            
            [self.POIParentNode addChildNode:POINode];
            //        NSLog(@"adding '%@' to scene", poi.textAsset.string);
        }
        
    }
    
}
- (void)setupPOIENUsWithCurrentLocation
{
    
//    int randomUL = 80;
//    
//    ENU a = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU b = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU c = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU d = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU e = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU f = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU g = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU h = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU i = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU j = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU k = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU l = {  arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0, arc4random_uniform(randomUL) - 40.0 };
//    ENU m = { 0, 50, 0 };
//    
//    NSArray *POIENUs = [[NSArray alloc] initWithObjects:
//                        [NSValue valueWithBytes:&a objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&b objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&c objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&d objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&e objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&f objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&g objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&h objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&i objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&j objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&k objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&l objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&m objCType:@encode(ENU)],nil];
//    
//    NSArray *POILabels = [[NSArray alloc] initWithObjects:
//                          @"O Captain!",
//                          @"my Captain",
//                          @"our fearful trip is done,",
//                          @"The ship has weather'd every rack,",
//                          @"the prize we sought is won,",
//                          @"The port is near,",
//                          @"the bells I hear,",
//                          @"the people all exulting,",
//                          @"While follow eyes,",
//                          @"the the steady keel,",
//                          @"the vessel",
//                          @"grim and",
//                          @"ENU @ (0,50,0)", nil];
//    
//    
    
    
    ENU a = { 0, 0, 80 };
    ENU b = { 0, 0, -80 };
//    ENU c = { 1000, 500, 0 };
//    ENU d = { 200, 50, -10 };
//    ENU e = { 100, 10, -10 };
    
    NSArray *POIENUs = [[NSArray alloc] initWithObjects:
                        [NSValue valueWithBytes:&a objCType:@encode(ENU)],
                        [NSValue valueWithBytes:&b objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&c objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&d objCType:@encode(ENU)],
//                        [NSValue valueWithBytes:&e objCType:@encode(ENU)],
                       nil];
    
    NSArray *POILabels = [[NSArray alloc] initWithObjects:
                          @"his patience at the center of the Ø",
                          @"a satellite view of the corpus",
//                          @"ENU @ (1000,500,0)",
//                          @"ENU @ (200, 50, -10)",
//                          @"ENU @ (100, 10, -10)",
                          nil];
    
    int textRotation = 90;
    
    LLA referenceLLA;
    referenceLLA.latitude = self.locationManager.location.coordinate.latitude;
    referenceLLA.longitude = self.locationManager.location.coordinate.longitude;
    referenceLLA.altitude = 0.0;
    
    LLA originLLA;
    
    if(IS_SITE_SPECIFIC) {
        originLLA = siteLLA;
    } else {
        originLLA = referenceLLA;
    }
    
    
    
    //NSLog(@"reference LLA - lat: %.4f lon: %.4f alt: %.4f", referenceLLA.latitude, referenceLLA.longitude, referenceLLA.altitude);
    
    for (NSUInteger i = 0; i < [POIENUs count]; i++) {
        
        // create a POI object
        ENU POIENU;
        [POIENUs[i] getValue:&POIENU];
        
        POI *poi = [[POI alloc] initWithReference:referenceLLA remoteOrigin:originLLA POIEnu:POIENU label:POILabels[i] textScale:8.0];
        
        // create a SCNNode for the POI with the POI textAsset
        SCNNode *POINode = [SCNNode nodeWithGeometry:poi.textAsset];
        
        // set the node's position and rotation (default faces the reference point)
        POINode.position = poi.POIXYZ;
        POINode.eulerAngles = [self EulersToLookAtReferenceFromPoint:poi.POIXYZ];
        SCNVector3 current =  POINode.eulerAngles;
        if(i == 1) textRotation = -textRotation;
        POINode.eulerAngles = SCNVector3Make(DEGREES_TO_RADIANS(textRotation), current.y, current.z);
        
        [self.POIParentNode addChildNode:POINode];
        //NSLog(@"adding '%@' to scene", poi.textAsset.string);
        
    }
    
}
- (void)setupReferenceAndPOIsUsingLLAs
{
    
    // Create hard-coded list of POI's.
    LLA a = {  41.879547,  -87.6256757, 0.0 };
    LLA b = {  41.883175,  -87.6218110, 0.0 };
    LLA c = {  42.0028128, -87.6677448, -20.0 };
    LLA d = {  27.936846,   86.9316905, 0.0 };
    LLA e = {  31.5368743,  35.2101009, 0.0 };
    LLA f = { -41.836828,   92.3730470, 0.0 };
    LLA g = {  42.005588, -87.657100, 10.0 };
    LLA h = {  42.007725, -87.668143, 20.0 };
    LLA i = {  41.998000,  -87.6622250, 0.0 };
    LLA j = {  41.903007, -87.707198, 0.0 };
    LLA k = {  41.895608, -87.690188, 185.0 };
    LLA l = { 41.895051, -87.687031,  0.0 };
    LLA m = { 41.969097, -87.657752,  0.0 };
    LLA n = { 41.909113, -87.677812, 0.0 };
    LLA o = {  41.913748, -87.675952, 4583};
    
    NSArray *POILLAs = [[NSArray alloc] initWithObjects:
                        [NSValue valueWithBytes:&a objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&b objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&c objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&d objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&e objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&f objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&g objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&h objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&i objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&j objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&k objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&l objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&m objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&n objCType:@encode(LLA)],
                        [NSValue valueWithBytes:&o objCType:@encode(LLA)],
                        nil];
    
    NSArray *POILabels = [[NSArray alloc] initWithObjects:
                          @"AIC",
                          @"Millenium Park Bandshell",
                          @"Abe's Rogers Park Apartment",
                          @"Mount Everest",
                          @"The Dead Sea",
                          @"The Opposite Side of the World",
                          @"Pratt Beach",
                          @"Common Cup",
                          @"Ellipsis Coffee",
                          @"Humboldt Park",
                          @"Star Lounge",
                          @"Dark Matter",
                          @"Aragon Ballrom",
                          @"the violet hour",
                          @"Placebo Point",
                          nil];
    
    // iterate over each POI and create a POI object
    LLA referenceLLA;
    referenceLLA.latitude = self.locationManager.location.coordinate.latitude;
    referenceLLA.longitude = self.locationManager.location.coordinate.longitude;
    NSLog(@"alt %f:",  self.locationManager.location.altitude);
    referenceLLA.altitude =0.0;

    POI *referencePOI = [[POI alloc]initWithReference:referenceLLA POI:referenceLLA label:@"reference POI" textScale:1.0];
    
    for (NSInteger i = 0; i < [POILLAs count]; i++) {
        
        LLA POILLA;
        [POILLAs[i] getValue:&POILLA];
        POI *poi = [[POI alloc] initWithReference:referenceLLA POI:POILLA label:POILabels[i] textScale:1.0];
        
        // if this is not the reference point, add textAsset to scene
        if (i != 0) {
            
            SCNNode *textNode = [SCNNode nodeWithGeometry:poi.textAsset];
            textNode.position = poi.POIXYZ;
            textNode.eulerAngles = [self EulersToLookAtReferenceFromPoint:textNode.position];
            [self.POIParentNode addChildNode:textNode];
            //NSLog(@"adding '%@' to scene", poi.textAsset.string);
            
        }
        
        [self.POIs addObject:poi];
        
    }
    
}
#pragma mark - Helper Methods
- (SCNVector3)EulersToLookAtReferenceFromPoint:(SCNVector3)POIXYZ
{
    
    double thetaY = atan2(POIXYZ.x, POIXYZ.z);
   // double thetaX = atan2(POIXYZ.y, POIXYZ.z);
    //    double thetaX = DEGREES_TO_RADIANS(30);
    //    double thetaZ = atan2(POIXYZ.x, POIXYZ.y);
    //    NSLog(@"thetaX: %.4f thetaY: %.4f thetaZ: %.4f", RADIANS_TO_DEGREES(thetaX), RADIANS_TO_DEGREES(thetaY), RADIANS_TO_DEGREES(thetaZ));
    SCNVector3 rotation = SCNVector3Make(DEGREES_TO_RADIANS(0), thetaY + DEGREES_TO_RADIANS(180), 0.0);
    //    SCNVector3 rotation = SCNVector3Make(0.0, thetaY + DEGREES_TO_RADIANS(180.0), thetaZ + DEGREES_TO_RADIANS(90.0));
    return rotation;
    
}
#pragma mark - Debug / Reference Mehods
- (void)setupReferenceTexts
{
    SCNText *n = [SCNText textWithString:@"N" extrusionDepth:.1];
    SCNText *e = [SCNText textWithString:@"E" extrusionDepth:.1];
    SCNText *s = [SCNText textWithString:@"S" extrusionDepth:.1];
    SCNText *w = [SCNText textWithString:@"W" extrusionDepth:.1];
    
    SCNNode *nNode = [SCNNode nodeWithGeometry:n];
    SCNNode *eNode = [SCNNode nodeWithGeometry:e];
    SCNNode *sNode = [SCNNode nodeWithGeometry:s];
    SCNNode *wNode = [SCNNode nodeWithGeometry:w];
    
    float distanceFromOrigin = 100;
    
    nNode.position = SCNVector3Make(0, 0, distanceFromOrigin * -1);
    eNode.position = SCNVector3Make(distanceFromOrigin, 0, 0);
    sNode.position = SCNVector3Make(0, 0, distanceFromOrigin);
    wNode.position = SCNVector3Make(distanceFromOrigin * -1, 0, 0);
    
    nNode.eulerAngles = [self EulersToLookAtReferenceFromPoint:nNode.position];
    eNode.eulerAngles = [self EulersToLookAtReferenceFromPoint:eNode.position];
    sNode.eulerAngles = [self EulersToLookAtReferenceFromPoint:sNode.position];
    wNode.eulerAngles = [self EulersToLookAtReferenceFromPoint:wNode.position];
    
    [self.POIParentNode addChildNode:nNode];
    [self.POIParentNode addChildNode:eNode];
    [self.POIParentNode addChildNode:sNode];
    [self.POIParentNode addChildNode:wNode];
    
}
#pragma mark - Inherited Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)radialText {
    NSArray *radialTexts = [[NSArray alloc] initWithObjects:
                          @"bound starlings",
                          @"in a trial",
                          @"of murmuration",
                          @"feet away",
                          @"from the zero",
                          nil];
    return radialTexts;

}

@end
