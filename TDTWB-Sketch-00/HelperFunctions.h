//
//  HelperFunctions.h *
//  CollocationsMockup
//
//  Created by Abraham Avnisan on 9/24/15.
//  Copyright (c) 2015 Abraham Avnisan. All rights reserved.
//

// * some of these helper functions are taken from the
//   openFrameworks core

#ifndef CollocationsMockup_HelperFunctions_h
#define CollocationsMockup_HelperFunctions_h

#import "stdlib.h"
@import Foundation;

// random number related functions
extern void  hfSeedRandom();
extern float hfRandom(float x, float y);
extern float hfRandomf();
extern float hfRandomPosF();

// mapping function
extern float hfMap(float input, float inputMin, float inputMax, float outputMin, float outputMax);
extern float hfMapClamped(float input, float inputMin, float inputMax, float outputMin, float outputMax);


#endif


