//
//  DSPFilter.h
//  TDTWB-Sketch-00
//
//  Created by Abraham Avnisan on 6/13/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, DSPFilterType) {
    DSPFilterLowPass,
    DSPFilterHighPass
};

@interface DSPFilter : NSObject

- (instancetype)initWithFilterType:(DSPFilterType)type;
- (instancetype)initWithFilterType:(DSPFilterType)type rate:(double)rate frequency:(double)frequency;
- (void)addValue:(double)newValue;

@property (nonatomic) double smoothed;
@property (nonatomic) double rate;
@property (nonatomic) double frequency;
@property (nonatomic) DSPFilterType filterType;

@end
