//
//  DSPFilter.m
//  TDTWB-Sketch-00
//
//  Created by Abraham Avnisan on 6/13/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import "DSPFilter.h"

@interface DSPFilter ()

@property (nonatomic) double filterConstant;
@property (nonatomic) double lastValue;

@end

@implementation DSPFilter

#pragma mark - initializers
- (instancetype)initWithFilterType:(DSPFilterType)type
{
    self = [super init];
    if (self) {
        
        self.filterType = type;
        
        if (type == DSPFilterHighPass) {
            
            // default rate and freq for high pass
            self.rate = 5.0;
            self.frequency = .1;
        } else if (type == DSPFilterLowPass) {
            
            // default rate and freq for low pass
            self.rate = 10;
            self.frequency = 2.5;

            
        }

        [self resetFilter];

    }
    return self;
}
- (instancetype)initWithFilterType:(DSPFilterType)type rate:(double)rate frequency:(double)frequency
{
    self = [super init];
    if (self) {
        self.filterType = type;
        self.rate = rate;
        self.frequency = frequency;
        [self resetFilter];
    }
    return self;
}
#pragma mark - override setters
- (void)setRate:(double)rate
{
    if (_rate != rate) {
        
        _rate = rate;
        [self resetFilter];
    
    }
}
- (void)setFrequency:(double)frequency
{
    if (_frequency != frequency) {
        
        _frequency = frequency;
        [self resetFilter];
    }
}
- (void)setFilterType:(DSPFilterType)filterType
{
    if (_filterType != filterType) {
        _filterType = filterType;
        [self resetFilter];
    }
}
#pragma mark - helper methods
- (void)resetFilter
{
    if (self.filterType == DSPFilterLowPass) {
        
        double dt = 1.0 / self.rate;
        double RC = 1.0 / self.frequency;
        self.filterConstant = dt / (dt + RC);
        
    } else if (self.filterType == DSPFilterHighPass) {
        
        double dt = 1.0 / self.rate;
        double RC = 1.0 / self.frequency;
        self.filterConstant = RC / (dt + RC);
        
    }
}
#pragma mark - main methods
- (void)addValue:(double)newValue
{
    double alpha = self.filterConstant;
    
    if (self.filterType == DSPFilterLowPass) {
        
        self.smoothed = newValue * alpha + self.smoothed * (1.0 - alpha);
        
    } else if (self.filterType == DSPFilterHighPass) {
        
        self.smoothed = alpha * (self.smoothed + newValue - self.lastValue);
        self.lastValue = newValue;
        
    }
}
@end
